#!/bin/bash

# Must be run as root
if [ "$(whoami)" != 'root' ]; then 
  printf "Please run this script as root\n"
  exit
fi

nmap -sP -PI -PT 192.168.0.1/24

host=192.168.0.20

my_ip=$(ip addr | grep -w inet | tail -1 | awk '{print $2}' | sed 's|/.*||g')
interface=$(ip addr | grep $my_ip -B 2 | head -1 | sed 's|: <.*||g' | sed 's/.*://g')

# Must be run as root
if [ -z  "$(ip addr | grep enp0s3 | grep promisc -i)" ]; then 
  ifconfig $interface promisc
fi

wsl=wireshark.log
nmap=nmap_output.log

rm -f $wsl
rm -f $nondns



tshark -l > $wsl &

# nmap -sN -T2 -sV 192.168.0.1/24 | tee $nmap
#sN/sF/sX
#nmap -sP -PI -PT 192.168.0.1/24 & sleep 10 ; kill $!
#nmap -PT -sX -T4 192.168.0.1/24 & sleep 10 ; kill $!

nmap -PT -sF -T4 192.168.0.1/24 > /dev/null 2>&1 & sleep 10 ; kill $!
nmap -PT -sN -T4 192.168.0.1/24 > /dev/null 2>&1 & sleep 10 ; kill $!
nmap -PT -sA -T4 192.168.0.1/24 > /dev/null 2>&1 & sleep 10 ; kill $!
nmap -PT -sX -T4 192.168.0.1/24 > /dev/null 2>&1 & sleep 10 ; kill $!


printf  "\n\nPress any key to continue.. \n\n"

read test;
while [ ! -z "$(ps -ef | grep nmap | grep -v grep)" ]; do 
  :
done

kill `ps -ef |grep tshar | awk '{print $2}'`

ifconfig $interface -promisc

printf "\n\nThere were $(grep "192.*192.*\[ACK\]" $wsl | wc -l) ack scans.\n"
grep "192.*192.*\[ACK\]" $wsl | grep "192.*192.*\[ACK\]" -i wireshark.log | tail -10

printf "\n\nThere were $(grep 'RST, ACK' -i $wsl | wc -l) FIN scans.\n"
grep 'RST, ACK' -i wireshark.log | tail -10

printf "\n\nThere were $(grep 'FIN, PSH, URG' $wsl | wc -l) Xmas tree scans.\n"
grep 'FIN, PSH, URG' -i wireshark.log | tail -10

printf "\n\nThere were $(grep '\[<None>\]' $wsl | wc -l) Null scans.\n"
grep '\[<None>\]' -i wireshark.log | tail -10


